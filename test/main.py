# -*- coding: utf-8 -*-
"""
Created on 25/03/2020
by William FLEITH
"""
import sys

# Insert in the first value of the PythonPath list the PYTHONPATH corresponding to this exercise.
sys.path.insert(0, "/home/tp/AGC_TP/tp1_ex4")
from calculator import SimpleCalculator

# main file
RES = SimpleCalculator(2, 1)
print(RES.sum())
print(RES.substract())
print(RES.multiply())
print(RES.divide())
